package RecipeRepository;
/*
 * File: RecipeRepositoryController.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This class is the controller for the Recipe Repository. It controls the data flow between all of the GUI
 * components and the Service. This is where the Caller anonymous class is created and passed to the GUI.
 */
import RecipeRepository.SharedEnumerationsAndUtilities.RecipeType;

import static RecipeRepository.SharedEnumerationsAndUtilities.*;

class RecipeRepositoryController {
    // class-scoped variables
    private RecipeRepositoryService service;
    private CallBack caller;

    // constructor which initializes the service and the GUI. Commented out method can be uncommented to seed the
    // database with dummy data
    RecipeRepositoryController() {
        initializeCaller();
        service = new RecipeRepositoryService();
        new RecipeRepositoryGUI(caller);
        // code for seeding test data. uncomment and start application to seed data. adjust "for" loop iterations
        // to change amount of recipes seeded
//        for(int i = 0; i < 100000; i++) {
//            service.createRecipe(new Recipe("recipe " + i, DUMMY_STEPS, RecipeType.getRandomRecipeType(),
//                    DUMMY_DESCRIPTION, DUMMY_INGREDIENTS, getRandomIntOrNull()), true);
//        }
    }

    // method that creates the anonymous class for the Caller, providing controls for the GUI
    private void initializeCaller() {
        caller = new CallBack() {
            @Override
            public String createCallBack(Recipe recipe) {
                return service.createRecipe(recipe, false);
            }

            @Override
            public PaginatedResult searchCallBack(String queryString, RecipeType recipeType, int pageNumber, SearchType searchType, Integer timeFilter, String ingredientFilter) {
                return service.searchRecipes(queryString, recipeType, pageNumber, searchType, timeFilter, ingredientFilter);
            }

            @Override
            public void deleteRecipe(String recipeName) {
                service.deleteRecipe(recipeName);
            }
        };
    }


}
