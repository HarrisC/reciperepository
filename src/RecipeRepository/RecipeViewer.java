package RecipeRepository;
/*
 * File: RecipeViewer.java
 * Date: 08 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This class creates a display for viewing Recipes, adn allows the user to delete the Recipe.
 */

import javax.swing.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import static RecipeRepository.SharedEnumerationsAndUtilities.createImageIcon;

class RecipeViewer extends JFrame {
    // class-scoped fields
    private Recipe recipe;
    private JPanel mainPanel;
    private CallBack caller;
    ActionListener actionListener;
    JButton button;

    // constructor which takes in a Recipe to be displayed, and a Callback for deleting
    RecipeViewer(Recipe recipe, CallBack caller, ActionListener actionListener, JButton button) {
        super("Recipe Viewer");
        this.recipe = recipe;
        this.caller = caller;
        this.actionListener = actionListener;
        this.button = button;
        setLayout(null);
        setBackground(Color.darkGray);
        setPreferredSize(new Dimension(900, 700));
        initComponents();
        pack();
        setLocationRelativeTo(null); //center the frame
        setContentPane(mainPanel);
        setResizable(false);
        setVisible(true);
    }

    // method for rendering the data of the Recipe
    private void initComponents() {
        mainPanel = new JPanel();
        mainPanel.setLayout(null);
        mainPanel.setSize(900, 700);

        // Adding the title
        JLabel name = new JLabel("<html><h2>" + recipe.name + "</h2></html>");
        name.setLocation(350, 10);
        name.setSize(200, 30);
        mainPanel.add(name);

        // Adding time estimation, if not null
        if (recipe.estimatedPrepAndCookTime != null) {
            JLabel time = new JLabel("<html><h3> " + recipe.estimatedPrepAndCookTime.toString()
                    + " minutes estimated prep and cook time</h3></html>");
            time.setLocation(560, 10);
            time.setSize(200, 40);
            mainPanel.add(time);
        }

        // Adding the description
        JLabel description = new JLabel("<html><i>" + recipe.description + "</i></html>");
        description.setLocation(200, 40);
        description.setSize(500, 100);
        mainPanel.add(description);

        // Adding Ingredients
        JLabel ingredientsLabel = new JLabel("<html><u><i>Ingredients</i></u></html>");
        ingredientsLabel.setLocation(20, 150);
        ingredientsLabel.setSize(100, 20);
        mainPanel.add(ingredientsLabel);
        recipe.ingredients.forEach(ingredient -> {
            JLabel ingredientText = new JLabel(ingredient);
            ingredientText.setSize(200, 40);
            ingredientText.setLocation(10, 160 + (45 * recipe.ingredients.indexOf(ingredient)));
            mainPanel.add(ingredientText);
        });

        // Adding Steps
        JLabel stepsLabel = new JLabel("<html><u><i>Steps</i></u></html>");
        stepsLabel.setLocation(240, 150);
        stepsLabel.setSize(100, 20);
        mainPanel.add(stepsLabel);
        recipe.steps.forEach(step -> {
            JLabel stepText = new JLabel((recipe.steps.indexOf(step) + 1) + ") " + step);
            stepText.setSize(400, 60);
            stepText.setLocation(200, 160 + (65 * recipe.steps.indexOf(step)));
            mainPanel.add(stepText);
        });

        // Adding delete button
        JButton deleteButton = new JButton(createImageIcon("images/delete.png"));
        deleteButton.setText("Delete");
        deleteButton.setBorderPainted(false);
        deleteButton.setFocusPainted(false);
        deleteButton.setSize(150, 50);
        deleteButton.setLocation(700, 550);
        deleteButton.addActionListener(e -> {
            this.caller.deleteRecipe(this.recipe.name);
            // trigger new event to handle refreshing results afterwards
            actionListener.actionPerformed(new ActionEvent(button, ActionEvent.ACTION_PERFORMED, null));
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });
        mainPanel.add(deleteButton);
    }
}
