package RecipeRepository;
/*
 * File: PaginatedResult.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This is a wrapper for the main model Recipe. It is an object that allows for easily paginating data.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class PaginatedResult {
    // necessary class-scoped variables
    private final static int RESULTS_PER_PAGE = 10;
    private int totalResults = 0, firstIndex, lastIndex, pageNumber;
    private boolean isFirstPage, isLastPage;
    private List<Recipe> truncatedResults;

    // constructor, which calculates the values for how the page will look
    PaginatedResult(int totalResults, int pageNumber, List<Recipe> results) {
        this.totalResults = results.size();
        this.pageNumber = pageNumber;
        this.firstIndex = RESULTS_PER_PAGE * (pageNumber - 1);
        this.isLastPage = pageNumber == ((totalResults / RESULTS_PER_PAGE) + 1);

        this.lastIndex = isLastPage ? results.size() - 1: RESULTS_PER_PAGE * pageNumber - 1;
        this.isFirstPage = pageNumber == 1;
        this.truncatedResults = new ArrayList<>();
        IntStream.range(firstIndex, lastIndex + 1).boxed().forEach(index -> {

            Recipe recipe = results.get(index);
            if (recipe != null) {
                truncatedResults.add(results.get(index));
            }
        });
    }

    // Getters
    public int getFirstIndex() {
        return firstIndex;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getLastIndex() {
        return lastIndex;
    }

    public boolean notFirstPage() {
        return !isFirstPage;
    }

    public boolean notLastPage() {
        return !isLastPage;
    }

    List<Recipe> getTruncatedResults() {
        return this.truncatedResults;
    }
}
