package RecipeRepository;

/*
 * File: Recipe.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This is the main Model of the application. It holds the data representing a Recipe.
 */

import RecipeRepository.SharedEnumerationsAndUtilities.RecipeType;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Recipe {

    // data fields of the Recipe model
    String name;
    String description;
    List<String> ingredients;
    List<String> steps;
    RecipeType recipeType;
    Integer estimatedPrepAndCookTime;

    // all args constructor
    Recipe(String name, List<String> steps, RecipeType recipeType, String description, List<String> ingredients, Integer estimatedPrepAndCookTime) {
        this.name = name;
        this.recipeType = recipeType;
        this.description = description;
        this.steps = steps;
        this.ingredients = ingredients;
        this.estimatedPrepAndCookTime = estimatedPrepAndCookTime;
    }

    // method for converting Recipe to String representation
    String toData() {
        String stepsString = steps.stream().map((step) -> "\"" + step + "\"").collect(Collectors.joining(","));
        String ingredientsString = ingredients.stream().map((ingredient) -> "\"" + ingredient + "\"").collect(Collectors.joining(","));
        return "{\"name\":\"" + this.name + "\", \"recipeType\":\"" + recipeType + "\", \"description\":\"" + description +
                "\", \"steps\":[" + stepsString + "], \"ingredients\":[" + ingredientsString + "], " +
                "\"estimatedPrepAndCookTime\":\"" + estimatedPrepAndCookTime +"\"}";
    }

    // method for parsing a line of data into a Recipe. still needs error handling
    static Recipe getRecipeFromData(String data) {
        String name = "";
        String description = "";
        List<String> ingredients = Collections.emptyList();
        List<String> steps = Collections.emptyList();
        RecipeType recipeType = null;
        Integer estimatedPrepAndCookTime = null;

        // regex to match string and array fields
        Pattern stringsPattern = Pattern.compile("(name|recipeType|description|estimatedPrepAndCookTime)\":\"((\\\\\"|[^\"])*)");
        Pattern arraysPattern = Pattern.compile("\"(steps|ingredients)\":\\[(.+?)]");

        Matcher stringsMatcher = stringsPattern.matcher(data);
        Matcher arraysMatcher = arraysPattern.matcher(data);
        // loop for dealing with the string matches
        while (stringsMatcher.find()) {
            // switching on the group 1, which is the field of Recipe
            switch (stringsMatcher.group(1)) {
                case "name":
                    name = stringsMatcher.group(2);
                    continue;
                case "recipeType":
                    if (Objects.equals(stringsMatcher.group(2), "null")) {
                        recipeType = null;
                    } else {
                        recipeType = RecipeType.valueOf(stringsMatcher.group(2));
                    }
                    continue;
                case "description":
                    description = stringsMatcher.group(2);
                    continue;
                case "estimatedPrepAndCookTime":
                    if (stringsMatcher.group(2).equals("null")) estimatedPrepAndCookTime = null;
                    else estimatedPrepAndCookTime = Integer.valueOf(stringsMatcher.group(2));
                default:
                    break;
            }
        }
        // loop for dealing with the array/list matches
        while (arraysMatcher.find()) {
            if ("steps".equals(arraysMatcher.group(1))) {
                steps = getListOfStringsFromData(arraysMatcher.group(2));
            } else if ("ingredients".equals(arraysMatcher.group(1))) {
                ingredients = getListOfStringsFromData(arraysMatcher.group(2));
            }
        }
        return new Recipe(name, steps, recipeType, description, ingredients, estimatedPrepAndCookTime);
    }

    // helper method for the array matcher loop
    private static List<String> getListOfStringsFromData(String group) {
        group = group.substring(1, group.length()-1);
        return Arrays.asList(group.split("\",\""));
    }

    public String toString() {
        return "<html><h3>Recipe Name: " + name + "</h3></html>";
    }

    // method used for searching to see if a string is within a recipe
    boolean contains(String queryString) {
        if (queryString == null) {
            return true;
        }
        if (containsQueryString(queryString, this.name)) {
            return true;
        }
        if (this.ingredients.stream().anyMatch((ingredient) ->
                containsQueryString(queryString, ingredient))) {
            return true;
        }
        if (containsQueryString(queryString, this.recipeType)) {
            return true;
        }
        if (containsQueryString(queryString, this.description)) {
            return true;
        }
        if (this.steps.stream().anyMatch((step) -> containsQueryString(queryString, step))) {
            return true;
        }
        return false;
    }

    private boolean containsQueryString(String queryString, Object p) {
        if (p == null) return false;
        String pattern = "";
        if (p instanceof RecipeType) {
            pattern = p.toString();
        } else if (p instanceof String){
            pattern = (String) p;
        }
        return Pattern.compile(Pattern.quote(queryString), Pattern.CASE_INSENSITIVE).matcher(pattern).find();
    }
}