package RecipeRepository;

/*
 * File: RecipeRepositoryGUI.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This class creates and controls the Graphical User Interface using Java Swing. The interactive
 * components call methods from the Caller interface. The main content is a JTabbedPane, with each inner panel
 * containing its own JPanel class (Create, Search, and Browse).
 */

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import java.awt.Color;
import java.awt.Dimension;

import static RecipeRepository.SharedEnumerationsAndUtilities.createImageIcon;

class RecipeRepositoryGUI extends JFrame {

    // class scoped variables
    private JTabbedPane tabbedNavigationPane;
    private CallBack caller;

    // constructor, takes a Caller for the interactive components
    RecipeRepositoryGUI(CallBack caller) {
        super("Recipe Repository");

        this.caller = caller;

        // using absolute layout
        setLayout(null);
        setBackground(Color.darkGray);
        setPreferredSize(new Dimension(700, 900));

        // method for initializing all of the components
        initComponents();
        // Set window size based on the preferred sizes of its contents.
        pack();
        //center
        setLocationRelativeTo(null);
        setContentPane(tabbedNavigationPane);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // End program when window closes.
        setVisible(true);
    }

    private void initComponents() {
        // experimenting with changing the lookk and feel of the tabbed pane
        UIManager.put("TabbedPane.selected", Color.GRAY);
        // creating the tabbed pane and the inner panels
        tabbedNavigationPane = new JTabbedPane();
        tabbedNavigationPane.setBackground(Color.lightGray);
        createSearchTab();
        createCreateTab();
        createBrowseTab();
    }

    // method for creating the Browse panel and attaching it to the tabbed pane
    private void createBrowseTab() {
        ImageIcon browseIcon = createImageIcon("images/browse.png");
        JPanel browsePanel = new BrowsePanel(caller);
        tabbedNavigationPane.addTab("<html><h2>Browse</h2></html>", browseIcon, browsePanel,
                "<html><h3>Browse all recipes</h3></html>");
    }

    // method for creating the Create panel and attaching it to the tabbed pane
    private void createCreateTab() {
        ImageIcon createIcon = createImageIcon("images/create.png");
        ImageIcon saveIcon = createImageIcon("images/save.png");
        JPanel createPanel = new CreatePanel(saveIcon, caller);
        tabbedNavigationPane.addTab("<html><h2>Create</h2></html>", createIcon, createPanel,
                "<html><h3>Create a new recipe</h3></html>");
        tabbedNavigationPane.setBackground(Color.LIGHT_GRAY);
    }

    // method for creating the Search panel and attaching it to the tabbed pane
    private void createSearchTab() {
        ImageIcon searchIcon = createImageIcon("images/search.png");
        JPanel searchPanel = new SearchPanel(caller);
        tabbedNavigationPane.addTab("<html><h2>Search</h2></html>", searchIcon, searchPanel,
                "<html><h3>Search through the recipe database</h3></html>");
    }
}