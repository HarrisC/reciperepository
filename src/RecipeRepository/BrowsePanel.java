package RecipeRepository;

/*
 * File: BrowsePanel.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This class creates and controls the Browse Panel, taking a Caller object for interactive components.
 * Uses three JLists, Breakfast, Lunch, and Dinner, which will be used for browsing recipes. This component relies on
 * the "Refresh" button to call the Service and update the respective Recipe lists.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

import RecipeRepository.SharedEnumerationsAndUtilities.RecipeType;
import static RecipeRepository.SharedEnumerationsAndUtilities.*;

class BrowsePanel extends JPanel {
    // class-scoped variables, mostly for state-management and pagination
    private JList breakfastList, lunchList, dinnerList;
    JButton breakfastPageLeftButton, breakfastPageRightButton, lunchPageLeftButton, lunchPageRightButton,
            dinnerPageLeftButton, dinnerPageRightButton;
    JLabel breakfastNumbersLabel, lunchNumbersLabel, dinnerNumbersLabel;
    private int breakfastPageNum = 1, lunchPageNum = 1, dinnerPageNum = 1;
    PaginatedResult breakfastRecipes, lunchRecipes, dinnerRecipes;
    CallBack caller;
    ActionListener refreshActionListener;

    // constructor, which still contains most of the logic for the component
    BrowsePanel(CallBack caller) {
        super(null);
        setBackground(Color.LIGHT_GRAY);
        this.caller = caller;
        // uses callback function to get the dummy data
        getBrowsingRecipes(null, 1);

        // actionListener for refreshing and deleting recipes
        refreshActionListener = e -> {
            breakfastPageNum = lunchPageNum = dinnerPageNum = 1;
            getBrowsingRecipes(null, 1);
        };

        // Adding breakfast header and list
        JLabel breakfastTopLabel = new JLabel("Breakfast");
        breakfastTopLabel.setSize(100, 30);
        breakfastTopLabel.setLocation(80, 10);
        add(breakfastTopLabel);

        // Adding breakfast numbers label
        breakfastNumbersLabel = new JLabel("<html><h3>No Breakfast Recipes</h3></html>");
        breakfastNumbersLabel.setLocation(70, 690);
        breakfastNumbersLabel.setSize(160, 40);
        add(breakfastNumbersLabel);

        // Adding breakfast pagination
        breakfastPageLeftButton = new JButton(createImageIcon("images/pageLeft.png"));
        breakfastPageLeftButton.addActionListener(e -> {
            breakfastPageNum--;
            getBrowsingRecipes(RecipeType.BREAKFAST, breakfastPageNum);
        });
        breakfastPageLeftButton.setLocation(60, 725);
        breakfastPageLeftButton.setSize(50, 40);
        breakfastPageLeftButton.setVisible(false);
        add(breakfastPageLeftButton);
        breakfastPageRightButton = new JButton(createImageIcon("images/pageRight.png"));
        breakfastPageRightButton.addActionListener(e -> {
            breakfastPageNum++;
            getBrowsingRecipes(RecipeType.BREAKFAST, breakfastPageNum);
        });
        breakfastPageRightButton.setLocation(120, 725);
        breakfastPageRightButton.setSize(50, 40);
        breakfastPageRightButton.setVisible(false);
        add(breakfastPageRightButton);

        // Adding lunch header and list
        JLabel lunchLabel = new JLabel("Lunch");
        lunchLabel.setSize(100, 30);
        lunchLabel.setLocation(315, 10);
        add(lunchLabel);

        // Adding lunch numbers label
        lunchNumbersLabel = new JLabel("<html><h3>No Lunch Recipes</h3></html>");
        lunchNumbersLabel.setLocation(290, 690);
        lunchNumbersLabel.setSize(150, 40);
        add(lunchNumbersLabel);

        // Adding lunch pagination
        lunchPageLeftButton = new JButton(createImageIcon("images/pageLeft.png"));
        lunchPageLeftButton.addActionListener(e -> {
            lunchPageNum--;
            getBrowsingRecipes(RecipeType.LUNCH, lunchPageNum);
        });
        lunchPageLeftButton.setLocation(285, 725);
        lunchPageLeftButton.setSize(50, 40);
        lunchPageLeftButton.setVisible(false);
        add(lunchPageLeftButton);
        lunchPageRightButton = new JButton(createImageIcon("images/pageRight.png"));
        lunchPageRightButton.addActionListener(e -> {
            lunchPageNum++;
            getBrowsingRecipes(RecipeType.LUNCH, lunchPageNum);
        });
        lunchPageRightButton.setLocation(350, 725);
        lunchPageRightButton.setSize(50, 40);
        lunchPageRightButton.setVisible(false);
        add(lunchPageRightButton);


        // Adding dinner header and list
        JLabel dinnerLabel = new JLabel("Dinner");
        dinnerLabel.setSize(100, 30);
        dinnerLabel.setLocation(550, 10);
        add(dinnerLabel);

        // Adding dinner numbers label
        dinnerNumbersLabel = new JLabel("<html><h3>No Dinner Recipes</h3></html>");
        dinnerNumbersLabel.setLocation(520, 690);
        dinnerNumbersLabel.setSize(150, 40);
        add(dinnerNumbersLabel);

        // Adding dinner pagination
        dinnerPageLeftButton = new JButton(createImageIcon("images/pageLeft.png"));
        dinnerPageLeftButton.addActionListener(e -> {
            dinnerPageNum--;
            getBrowsingRecipes(RecipeType.DINNER, dinnerPageNum);
        });
        dinnerPageLeftButton.setLocation(520, 725);
        dinnerPageLeftButton.setSize(50, 40);
        dinnerPageLeftButton.setVisible(false);
        add(dinnerPageLeftButton);
        dinnerPageRightButton = new JButton(createImageIcon("images/pageRight.png"));
        dinnerPageRightButton.addActionListener(e -> {
            dinnerPageNum++;
            getBrowsingRecipes(RecipeType.DINNER, dinnerPageNum);
        });
        dinnerPageRightButton.setLocation(585, 725);
        dinnerPageRightButton.setSize(50, 40);
        dinnerPageRightButton.setVisible(false);
        add(dinnerPageRightButton);

        // Adding refresh button
        JButton refreshButton = new JButton("Refresh");
        refreshButton.setSize(100, 30);
        refreshButton.setLocation(300, 770);
        refreshButton.addActionListener(refreshActionListener);
        add(refreshButton);
    }

    // method to asynchronously get the browsing recipes while displaying a "please wait" message;
    private void getBrowsingRecipes(RecipeType recipeType, int pageNum) {
        JDialog jDialog = getWaitingDialog();


        // creating a worker to retrieve the data from the database concurrently
        // code example taken from https://docs.oracle.com/javase/tutorial/uiswing/concurrency/simple.html
        SwingWorker worker = new SwingWorker<Void, Void>() {
            @Override
            public Void doInBackground() {
                try {
                    if (recipeType != null) {
                        if (recipeType == RecipeType.BREAKFAST) {
                            breakfastRecipes = caller.searchCallBack("", RecipeType.BREAKFAST, pageNum, SearchType.BROWSING_BREAKFAST, null, null);
                            createJLists(Arrays.asList(breakfastRecipes, lunchRecipes, dinnerRecipes));
                        } else if (recipeType == RecipeType.LUNCH) {
                            lunchRecipes = caller.searchCallBack("", RecipeType.LUNCH, pageNum, SearchType.BROWSING_LUNCH, null, null);
                            createJLists(Arrays.asList(breakfastRecipes, lunchRecipes, dinnerRecipes));
                        } else {
                            dinnerRecipes = caller.searchCallBack("", RecipeType.DINNER, pageNum, SearchType.BROWSING_DINNER, null, null);
                            createJLists(Arrays.asList(breakfastRecipes, lunchRecipes, dinnerRecipes));
                        }
                    } else {
                        breakfastRecipes = caller.searchCallBack("", RecipeType.BREAKFAST, pageNum, SearchType.BROWSING_BREAKFAST, null, null);
                        lunchRecipes = caller.searchCallBack("", RecipeType.LUNCH, pageNum, SearchType.BROWSING_LUNCH, null, null);
                        dinnerRecipes = caller.searchCallBack("", RecipeType.DINNER, pageNum, SearchType.BROWSING_DINNER, null, null);
                        createJLists(Arrays.asList(breakfastRecipes, lunchRecipes, dinnerRecipes));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void done() {
                jDialog.dispose();
            }
        };
        worker.execute();
        revalidate();
        repaint();
    }




    // method used for creating and setting up the JLists, along with the pagination
    private void createJLists(List<PaginatedResult> data) {
        // configuring breakfast list
        if (breakfastList == null) {
            breakfastList = new JList();
            breakfastList.setLocation(5, 45);
            breakfastList.setSize(220, 650);
            breakfastList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
            breakfastList.setLayoutOrientation(JList.VERTICAL);
            breakfastList.setBorder(BorderFactory.createLineBorder(Color.black));
            breakfastList.setFixedCellHeight(60);
            breakfastList.addListSelectionListener(e -> {
                JList source = (JList) e.getSource();
                if (!source.getValueIsAdjusting() && source.getSelectedValue() != null) {
                    new RecipeViewer((Recipe) source.getSelectedValue(), this.caller, refreshActionListener, breakfastPageLeftButton);
                }
            });
            breakfastList.setVisible(true);
            add(breakfastList);
        }
        breakfastList.setListData(data.get(0).getTruncatedResults().toArray());


        // configuring lunch list
        if (lunchList == null) {
            lunchList = new JList();
            lunchList.setLocation(235, 45);
            lunchList.setSize(220, 650);
            lunchList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
            lunchList.setLayoutOrientation(JList.VERTICAL);
            lunchList.setBorder(BorderFactory.createLineBorder(Color.black));
            lunchList.setFixedCellHeight(60);
            lunchList.addListSelectionListener(e -> {
                JList source = (JList) e.getSource();
                if (!source.getValueIsAdjusting() && source.getSelectedValue() != null) {
                    new RecipeViewer((Recipe) source.getSelectedValue(), this.caller, refreshActionListener, lunchPageLeftButton);
                }
            });
            lunchList.setVisible(true);
            add(lunchList);
        }
        lunchList.setListData(data.get(1).getTruncatedResults().toArray());


        // configuring dinner list
        if (dinnerList == null) {
            dinnerList = new JList();
            dinnerList.setLocation(465, 45);
            dinnerList.setSize(220, 650);
            dinnerList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
            dinnerList.setLayoutOrientation(JList.VERTICAL);
            dinnerList.setBorder(BorderFactory.createLineBorder(Color.black));
            dinnerList.setFixedCellHeight(60);
            dinnerList.addListSelectionListener(e -> {
                JList source = (JList) e.getSource();
                if (!source.getValueIsAdjusting() && source.getSelectedValue() != null) {
                    new RecipeViewer((Recipe) source.getSelectedValue(), this.caller, refreshActionListener, dinnerPageLeftButton);
                }
            });
            dinnerList.setVisible(true);
            add(dinnerList);
        }
        dinnerList.setListData(data.get(2).getTruncatedResults().toArray());



        if (breakfastRecipes.getTotalResults() > 0) {
            breakfastPageLeftButton.setVisible(breakfastRecipes.notFirstPage());
            breakfastPageRightButton.setVisible(breakfastRecipes.notLastPage());
            breakfastNumbersLabel.setText("<html><h3>Recipes " +
                    (breakfastRecipes.getFirstIndex() + 1) + "-" + (breakfastRecipes.getLastIndex() + 1) + "</h3></html>");
        }
        if (lunchRecipes.getTotalResults() > 0) {
            lunchPageLeftButton.setVisible(lunchRecipes.notFirstPage());
            lunchPageRightButton.setVisible(lunchRecipes.notLastPage());
            lunchNumbersLabel.setText("<html><h3>Recipes " +
                    (lunchRecipes.getFirstIndex() + 1) + "-" + (lunchRecipes.getLastIndex() + 1) + "</h3></html>");
        }
        if (dinnerRecipes.getTotalResults() > 0) {
            dinnerPageLeftButton.setVisible(dinnerRecipes.notFirstPage());
            dinnerPageRightButton.setVisible(dinnerRecipes.notLastPage());
            dinnerNumbersLabel.setText("<html><h3>Recipes " +
                    (dinnerRecipes.getFirstIndex() + 1) + "-" + (dinnerRecipes.getLastIndex() + 1) + "</h3></html>");
        }


    }
}
