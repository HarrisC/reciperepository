package RecipeRepository;
/*
 * File: SearchPanel.java
 * Date: 08 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This class creates the Search Panel for recipes. It currently just takes a query string and searches with
 * it. Will display a message "Please Wait..." while results are loading.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static RecipeRepository.SharedEnumerationsAndUtilities.*;

class SearchPanel extends JPanel {
    // class-scoped fields
    private JTextField searchInput, ingredientFilter;
    private JFormattedTextField timeFilter;
    Integer timeFilterValue;
    String ingredientFilterValue;
    private JList resultsList;
    private PaginatedResult paginatedResults;
    private List<Recipe> results;
    private JLabel resultsTotalsLabel;
    JButton pageLeftButton, pageRightButton, searchButton;
    private int pageNumber = 1;
    private ActionListener actionListener;
    private CallBack caller;

    // constructor takes a Caller for searching
    SearchPanel(CallBack caller) {
        super(null);
        this.caller = caller;
        //action listener for pagination
        createActionListener();
        setBackground(Color.LIGHT_GRAY);
        results = new ArrayList<>();

        // adding results label
        JLabel resultsLabel = new JLabel("<html><h1>Results</h1></html>");
        resultsLabel.setLocation(300, 60);
        resultsLabel.setSize(100, 40);
        add(resultsLabel);

        // adding results totals label
        resultsTotalsLabel = new JLabel("<html><h3 style=\"width:200px;text-indent:45px\">No recipes found</h3></html>");
        resultsTotalsLabel.setLocation(225, 100);
        resultsTotalsLabel.setSize(240, 40);
        resultsTotalsLabel.setVisible(false);
        add(resultsTotalsLabel);


        // adding search button
        searchButton = new JButton("Search");
        searchButton.setLocation(510, 10);
        searchButton.setSize(100, 40);
        searchButton.addActionListener(actionListener);
        add(searchButton);

        // adding clear search button
        JButton clearSearchButton = new JButton("Clear");
        clearSearchButton.setLocation(510, 125);
        clearSearchButton.setSize(100, 20);
        clearSearchButton.addActionListener(e -> {
            results = new ArrayList<>();
            searchInput.setText("");
            timeFilter.setValue(null);
            ingredientFilter.setText("");
            resultsTotalsLabel.setVisible(false);
            pageLeftButton.setVisible(false);
            pageRightButton.setVisible(false);
            createJList(false);
        });
        add(clearSearchButton);

        // adding search text field input
        searchInput = new JTextField();
        searchInput.setLocation(100, 10);
        searchInput.setSize(400, 40);
        add(searchInput);

        // adding cook time field input for filter, formatted for numbers only
        JLabel timeFilterLabel = new JLabel("<html><h3>Max cook time:</h3></html>");
        timeFilterLabel.setSize(140, 40);
        timeFilterLabel.setLocation(445, 50);
        add(timeFilterLabel);
        timeFilter = new JFormattedTextField(getNumberFormatter());
        timeFilter.setSize(100, 25);
        timeFilter.setLocation(550, 55);
        add(timeFilter);

        // adding ingredient filter
        JLabel ingredientFilterLabel = new JLabel("<html><h3>Must have ingredient:</h3></html>");
        ingredientFilterLabel.setSize(160, 40);
        ingredientFilterLabel.setLocation(395, 80);
        add(ingredientFilterLabel);
        ingredientFilter = new JTextField();
        ingredientFilter.setSize(100, 25);
        ingredientFilter.setLocation(550, 85);
        add(ingredientFilter);

        // initial setting of the results JList
        createJList(true);
        resultsList.addListSelectionListener(e -> {
            JList source = (JList) e.getSource();
            if (!source.getValueIsAdjusting() && source.getSelectedValue() != null) {
                new RecipeViewer((Recipe) source.getSelectedValue(), this.caller, actionListener, searchButton);
            }
        });
        add(resultsList);

        // adding page left
        pageLeftButton = new JButton(createImageIcon("images/pageLeft.png"));
        pageLeftButton.addActionListener(actionListener);
        pageLeftButton.setLocation(300, 705);
        pageLeftButton.setSize(50, 40);
        pageLeftButton.setVisible(false);
        add(pageLeftButton);

        // adding page right
        pageRightButton = new JButton(createImageIcon("images/pageRight.png"));
        pageRightButton.addActionListener(actionListener);
        pageRightButton.setLocation(350, 705);
        pageRightButton.setSize(50, 40);
        pageRightButton.setVisible(false);
        add(pageRightButton);




    }

    // method for initializing the action listener used for pagination
    private void createActionListener() {
        actionListener = e -> {
            if (e.getSource() == pageLeftButton) {
                pageNumber--;
            }
            if (e.getSource() == pageRightButton) {
                pageNumber++;
            }
            if (e.getSource() == searchButton) {
                timeFilterValue = (Integer) timeFilter.getValue();
                ingredientFilterValue = ingredientFilter.getText();
                pageNumber = 1;
            }

            JDialog jDialog = getWaitingDialog();

            // creating a worker to retrieve the data from the database concurrently
            // code example taken from https://docs.oracle.com/javase/tutorial/uiswing/concurrency/simple.html
            SwingWorker worker = new SwingWorker<Void, Void>() {
                @Override
                public Void doInBackground() {
                    try {
                        paginatedResults = caller.searchCallBack(searchInput.getText(), null, pageNumber, SearchType.SEARCH, timeFilterValue, ingredientFilterValue);
                        results = paginatedResults.getTruncatedResults();
                        if (paginatedResults.getTotalResults() > 0) {
                            if (paginatedResults.notFirstPage()) {
                                pageLeftButton.setVisible(true);
                            } else {
                                pageLeftButton.setVisible(false);
                            }
                            if (paginatedResults.notLastPage()) {
                                pageRightButton.setVisible(true);
                            } else {
                                pageRightButton.setVisible(false);

                            }
                            resultsTotalsLabel.setText("<html><h3>Recipes " + (paginatedResults.getFirstIndex() + 1) + "-"
                                    + (paginatedResults.getLastIndex() + 1) + " of " + paginatedResults.getTotalResults()
                                    + " total results</h3></html>");
                            resultsTotalsLabel.setVisible(true);
                        } else {
                            resultsTotalsLabel.setText("<html><h3 style=\"width:200px;text-indent:45px\">No recipes found</h3></html>");
                            resultsTotalsLabel.setVisible(true);
                            pageLeftButton.setVisible(false);
                            pageRightButton.setVisible(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                public void done() {
                    createJList(false);
                    jDialog.dispose();
                }
            };
            worker.execute();
        };
    }

    // method for making and updating the results JList
    private void createJList(boolean isNew) {
        if (isNew) {
            resultsList = new JList();
            resultsList.setLocation(100, 150);
            resultsList.setSize(500, 550);
            resultsList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
            resultsList.setLayoutOrientation(JList.VERTICAL);
            resultsList.setBorder(BorderFactory.createLineBorder(Color.black));
        }
        resultsList.setFixedCellHeight(50);

        resultsList.setListData(results.toArray());
        revalidate();
        repaint();
    }
}
