package RecipeRepository;
/*
 * File: CreatePanel.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This class creates and controls the Create Panel, taking a Caller object for interactive components, and
 * an ImageIcon for for the save button. Generates a simple form using Swing components for entering a Recipe
 * and saving it. When saving, it uses the response from the Service to indicate whether the save was successful
 * or if a duplicate Recipe was found.
 */
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import RecipeRepository.SharedEnumerationsAndUtilities.RecipeType;

import static RecipeRepository.SharedEnumerationsAndUtilities.getNumberFormatter;

class CreatePanel extends JPanel {
    // necessary class-scope variables
    private JTextField recipeNameField;
    private JFormattedTextField prepAndCookTimeField;
    private JTextArea descriptionTextArea;
    private JPanel ingredientsPanel;
    private ArrayList<JTextField> ingredients;
    private JButton addIngredientsButton;
    private JPanel stepsPanel;
    private ArrayList<JTextArea> steps;
    private JButton addStepsButton;
    ButtonGroup radioButtonGroup;
    JRadioButton breakfastRadio, lunchRadio, dinnerRadio;

    CreatePanel(ImageIcon saveIcon, CallBack onSave) {
        super(null);

        // Adding Title input and label
        JLabel recipeNameLabel = new JLabel("<html><h2>Recipe Name</h2></html>");
        recipeNameLabel.setLocation(80, 4);
        recipeNameLabel.setSize(150, 30);
        recipeNameField = new JTextField();
        recipeNameField.setLocation(40, 40);
        recipeNameField.setSize(190, 20);
        add(recipeNameLabel);
        add(recipeNameField);

        // Adding Recipe Type radios
        breakfastRadio = new JRadioButton("Breakfast");
        breakfastRadio.setSize(90, 40);
        breakfastRadio.setLocation(20, 60);
        lunchRadio = new JRadioButton("Lunch");
        lunchRadio.setSize(90, 40);
        lunchRadio.setLocation(110, 60);
        dinnerRadio = new JRadioButton("Dinner");
        dinnerRadio.setSize(120, 40);
        dinnerRadio.setLocation(180, 60);
        radioButtonGroup = new ButtonGroup();
        radioButtonGroup.add(breakfastRadio);
        radioButtonGroup.add(lunchRadio);
        radioButtonGroup.add(dinnerRadio);
        add(breakfastRadio);
        add(lunchRadio);
        add(dinnerRadio);

        // Adding Description text area and label
        JLabel descriptionLabel = new JLabel("<html><h2>Description</h2></html>");
        descriptionLabel.setLocation(450, 4);
        descriptionLabel.setSize(150, 30);
        descriptionTextArea = new JTextArea();
        descriptionTextArea.setLocation(325, 30);
        descriptionTextArea.setSize(325, 75);
        descriptionTextArea.setLineWrap(true);
        descriptionTextArea.setBorder(BorderFactory.createLineBorder(Color.black));
        add(descriptionLabel);
        add(descriptionTextArea);

        // Adding ingredients
        ingredientsPanel = new JPanel(null);
        ingredientsPanel.setLocation(5, 110);
        ingredientsPanel.setSize(290, 650);
        ingredientsPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        JLabel ingredientLabel = new JLabel("<html><h2>Ingredients</h2></html>");
        ingredientLabel.setLocation(80, 10);
        ingredientLabel.setSize(100, 25);
        ingredientsPanel.add(ingredientLabel);
        ingredients = new ArrayList<>();
        ingredients.add(getIngredientInput());

        ingredients.forEach((ingredient) -> {
            ingredient.setLocation(10, 40 + (ingredients.indexOf(ingredient) * 25));
            ingredientsPanel.add(ingredient);
        });
        addIngredientsButton = new JButton("Add Ingredient");
        addIngredientsButton.addActionListener(e -> {
            if (ingredients.size() < 23) {
                ingredients.add(getIngredientInput());
                ingredients.forEach((ingredient) -> {
                    ingredient.setLocation(10, 40 + (ingredients.indexOf(ingredient) * 25));
                    ingredientsPanel.add(ingredient);

                });
                addIngredientsButton.setLocation(50, 50 + (ingredients.size() * 25));
                revalidate();
                repaint();
            }
        });
        addIngredientsButton.setSize(150, 20);
        addIngredientsButton.setLocation(50, 50 + (ingredients.size() * 25));
        ingredientsPanel.add(addIngredientsButton);
        add(ingredientsPanel);

        // Adding steps
        stepsPanel = new JPanel(null);
        stepsPanel.setLocation(320, 110);
        stepsPanel.setSize(340, 650);
        stepsPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        JLabel stepsLabel = new JLabel("<html><h2>Steps</h2></html>");
        stepsLabel.setLocation(140, 10);
        stepsLabel.setSize(100, 20);
        stepsPanel.add(stepsLabel);
        steps = new ArrayList<>();
        steps.add(getStepsInput());
        steps.forEach((step) -> {
            step.setLocation(60, 40 + (steps.indexOf(step) * 65));
            JLabel stepLabel = new JLabel("Step " + (steps.indexOf(step) + 1));
            stepLabel.setLocation(5, 60 + (steps.indexOf(step) * 65));
            stepLabel.setSize(50, 20);
            stepsPanel.add(stepLabel);
            stepsPanel.add(step);
        });
        addStepsButton = new JButton("Add Step");
        addStepsButton.setLocation(85, 50 + (steps.size() * 65));
        addStepsButton.addActionListener(e -> {
            if (steps.size() < 9) {
                steps.add(getStepsInput());
                steps.forEach((step) -> {
                    step.setLocation(85, 40 + (steps.indexOf(step) * 65));
                    JLabel stepLabel = new JLabel("Step " + (steps.indexOf(step) + 1));
                    stepLabel.setLocation(5, 60 + (steps.indexOf(step) * 65));
                    stepLabel.setSize(50, 20);
                    stepsPanel.add(stepLabel);
                    stepsPanel.add(step);

                });
                addStepsButton.setLocation(85, 50 + (steps.size() * 65));
                revalidate();
                repaint();
            }
        });
        addStepsButton.setSize(150, 20);
        stepsPanel.add(addStepsButton);
        add(stepsPanel);

        // Adding Estimated Prep and Cook time field, formatted for numbers only
        JLabel prepAndCookTimeLabel = new JLabel("<html><h4>Estimated Prep and Cook Time (minutes only):</h4></html>");
        prepAndCookTimeLabel.setSize(110, 100);
        prepAndCookTimeLabel.setLocation(20, 732);
        add(prepAndCookTimeLabel);
        prepAndCookTimeField = new JFormattedTextField(getNumberFormatter());
        prepAndCookTimeField.setSize(75, 30);
        prepAndCookTimeField.setLocation(120, 770);
        add(prepAndCookTimeField);

        // Adding Save button, which handles duplicates found from service
        JButton saveButton = new JButton("<html><h3>Save</h3></html>", saveIcon);
        saveButton.setSize(100, 35);
        saveButton.setLocation(250, 770);
        saveButton.addActionListener(e -> {
            String name = recipeNameField.getText();
            List<String> stepsText = steps.stream().map(JTextArea::getText).collect(Collectors.toList());
            RecipeType recipeType = getRecipeType();
            String description = descriptionTextArea.getText();
            List<String> ingredientsText = ingredients.stream().map(JTextField::getText).collect(Collectors.toList());
            Integer estimatedPrepAndCookTime = (Integer) prepAndCookTimeField.getValue();
            Recipe recipe = new Recipe(name, stepsText, recipeType, description, ingredientsText, estimatedPrepAndCookTime);
            String result = onSave.createCallBack(recipe);
            if ("duplicate found".equals(result)){
                JDialog jDialog = new JDialog();
                jDialog.add(new JLabel("<html><h3>Recipe with this name" +
                        " already exists</h3></html>"));
                jDialog.setMinimumSize(new Dimension(300, 150));
                jDialog.setResizable(false);
                jDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                jDialog.setLocationRelativeTo(null);
                jDialog.setVisible(true);
            }
        });
        add(saveButton);
    }

    // method for getting which recipe type is selected
    private RecipeType getRecipeType() {
        if (breakfastRadio.isSelected()) return RecipeType.BREAKFAST;
        if (lunchRadio.isSelected()) return RecipeType.LUNCH;
        if (dinnerRadio.isSelected()) return RecipeType.DINNER;
        return null;
    }

    // method for getting a new recipe step input
    private JTextArea getStepsInput() {
        JTextArea jTextArea = new JTextArea();
        jTextArea.setLineWrap(true);
        jTextArea.setBorder(BorderFactory.createLineBorder(Color.lightGray));
        jTextArea.setSize(200, 60);
        return jTextArea;
    }

    // method for getting a new ingredient input
    private JTextField getIngredientInput() {
        JTextField jTextField = new JTextField();
        jTextField.setSize(270, 20);
        jTextField.setBorder(BorderFactory.createLineBorder(Color.black));
        return jTextField;
    }
}
