package RecipeRepository;
/*
 * File: CallBack.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This interface is used to allow the Controller to create an anonymous class, and allow the
 * GUI to call these functions with interactions.
 */
import RecipeRepository.SharedEnumerationsAndUtilities.RecipeType;

import static RecipeRepository.SharedEnumerationsAndUtilities.*;


public interface CallBack {
    String createCallBack(Recipe recipe);

    PaginatedResult searchCallBack(String queryString, RecipeType recipeType, int pageNumber, SearchType searchType, Integer timeFilter, String ingredientFilter);

    void deleteRecipe(String recipeName);
}
