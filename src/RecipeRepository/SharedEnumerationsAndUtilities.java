package RecipeRepository;
/*
 * File: SharedEnumerationsAndUtilities.java
 * Date: 08 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This is a utility class that contains dummy data, enumerations, and other shared utilities.
 */

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.net.URL;
import java.security.SecureRandom;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class SharedEnumerationsAndUtilities {
    private static final Random RAND = new SecureRandom();
    // enumerated Recipe types for standardizing data
    public enum RecipeType {
        BREAKFAST, LUNCH, DINNER;

        // static method for getting a random recipe type
        public static RecipeType getRandomRecipeType() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }

    public enum SearchType {
        BROWSING_BREAKFAST, BROWSING_LUNCH, BROWSING_DINNER, SEARCH;
    }

    // enumerated recipe type index representation
    public enum RecipeTypeIndex {
        BREAKFAST(0), LUNCH(1), DINNER(2);
        private int index;

        RecipeTypeIndex(int index) {
            this.index = index;
        }

        public int getRecipeTypeIndex() {
            return this.index;
        }
    }

    // dummy data for populating components, in use until production features are implemented
    final static String DUMMY_DESCRIPTION = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor" +
            " incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation" +
            " ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit" +
            " in voluptate velit esse cillum dolore eu fugiat nulla pariatur. HODOR IPSUM";
    final static List<String> DUMMY_INGREDIENTS = Arrays.asList("1 Cup Brown Sugar", "1 Pinch Salt", "1 TSP Pepper", "2 lbs. Chicken");
    final static List<String> DUMMY_STEPS = Arrays.asList("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do" +
                    " eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis" +
                    " nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            "Cook onions until translucent", "Oil, salt and pepper the chicken", "Wrap the chicken breasts in bacon.");

    // number formatter for prep and cooking time. minimum 0 minutes (can't do negative time)
    // code derived from https://docs.oracle.com/javase/8/docs/api/javax/swing/text/NumberFormatter.html
    public static NumberFormatter getNumberFormatter() {
        NumberFormat timeFormat = NumberFormat.getNumberInstance();
        NumberFormatter formatter = new NumberFormatter(timeFormat);
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(0);
        formatter.setAllowsInvalid(false);
        return formatter;
    }

    public static Integer getRandomIntOrNull() {
        int randomNumber = RAND.nextInt(60);
        if (randomNumber == 0 || randomNumber == 60 || (5 < randomNumber && randomNumber < 15) || (30 < randomNumber && randomNumber < 45)) {
            return null;
        }
        return randomNumber;
    }

    // static method for getting an image icon, used for setting Icons in the JTabbedPane
    static ImageIcon createImageIcon(String path) {
        URL imgURL = RecipeRepositoryGUI.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file with path: " + path);
            return null;
        }
    }

    // static method for getting a "Please Wait" popup
    static JDialog getWaitingDialog() {
        JDialog jDialog = new JDialog();
        jDialog.add(new JLabel("<html><h2 style=\"width:150px;text-indent:35px\">Please wait...</span></html>"));
        jDialog.setMinimumSize(new Dimension(200, 50));
        jDialog.setResizable(false);
        jDialog.setUndecorated(true);
        jDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        jDialog.setLocationRelativeTo(null);
        jDialog.setVisible(true);
        return jDialog;
    }
}
