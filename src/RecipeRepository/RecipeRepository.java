package RecipeRepository;
/*
 * File: RecipeRepository.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This class only contains the main method, which creates the Recipe Repository.
 */

import javax.swing.SwingUtilities;

public class RecipeRepository {

    // main method
    public static void main(String[] args) {
        SwingUtilities.invokeLater(RecipeRepositoryController::new);
    }
}
