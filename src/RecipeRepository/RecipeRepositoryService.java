package RecipeRepository;
/*
 * File: RecipeRepositoryService.java
 * Date: 12 December 2019
 * Author: Clinton Harris, Alex Peksa, Timothy Kershaw, and Ali Shamim
 * Purpose: This class is a service for the Recipe Repository. It connects to or creates a new database, which is
 * just a text file at this point. This service holds the functionality for writing a new recipe to the database,
 * and reading Recipes from the database, as well as searching for particular words in a Recipe and searching by
 * Recipe Type. Also allows for deleting a recipe.
 */

import RecipeRepository.SharedEnumerationsAndUtilities.RecipeType;

import static RecipeRepository.SharedEnumerationsAndUtilities.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


class RecipeRepositoryService {
    // database field
    private File database;
    // cache to enhance pagination
    private Map<SearchType, List<Recipe>> resultsCache;

    // constructor which calls a method for connecting to a database
    RecipeRepositoryService() {
        resultsCache = new HashMap<>();
        connectToDatabase();
    }

    // method for either connecting to or creating a new database file
    private void connectToDatabase() {
        database = new File("recipeRepository.txt");

        try {
            if (database.createNewFile()) {
                System.out.println("New database");
            } else {
                System.out.println("Database already exists.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // method for writing a Recipe to the database
    String createRecipe(Recipe recipe, boolean noDeDup) {
        // searching for recipes with the same name, duplicates
        // doesn't dedup if coming from seeder in controller
        if (!noDeDup) {
            List<Recipe> results = getCurrentRecipes();

            for (Recipe resultRecipe : results) {
                if (resultRecipe.name.equals(recipe.name)) {
                    return "duplicate found";
                }
            } // end deduping
        }

        // writing the recipe if not a duplicate
        try {
            FileWriter writer = new FileWriter(database, true);
            writer.write(recipe.toData());
            writer.write("\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // method for searching through all of the recipes by a query string, Recipe type, time filter, or ingredient filter
    // returns paginated results
    PaginatedResult searchRecipes(String queryString, RecipeType recipeType, int pageNumber, SearchType cache, Integer timeFilter, String ingredientFilter) {
        if (resultsNotCached(cache, pageNumber)) {
            List<Recipe> results = getCurrentRecipes();

            results = results.stream().filter((recipe -> {
                boolean returnBoolean = recipe.contains(queryString);
                if (recipeType != null) {
                    returnBoolean = returnBoolean && recipeType.equals(recipe.recipeType);
                }
                if (timeFilter != null) {
                    if (recipe.estimatedPrepAndCookTime == null) returnBoolean = false;
                    else returnBoolean = returnBoolean && (recipe.estimatedPrepAndCookTime <= timeFilter);
                }
                if (ingredientFilter != null && !"".equals(ingredientFilter)) {
                    boolean ingredientBoolean = false;
                    for (String ingredient : recipe.ingredients) {
                        if (ingredient.toLowerCase().contains(ingredientFilter.toLowerCase())) {
                            ingredientBoolean = true;
                            break;
                        }
                    }
                    returnBoolean = returnBoolean && ingredientBoolean;
                }
                return returnBoolean;
            })).collect(Collectors.toList());
            // caching results
            resultsCache.put(cache, results);
        }
        return new PaginatedResult(resultsCache.get(cache).size(), pageNumber, resultsCache.get(cache));
    }

    // method for getting all of the recipes from the database
    private List<Recipe> getCurrentRecipes() {
        List<Recipe> results = new ArrayList<>();
        try (Scanner scanner = new Scanner(database)) {
            scanner.useDelimiter("}");
            while (scanner.hasNextLine()) {
                Recipe recipe = Recipe.getRecipeFromData(scanner.nextLine());
                results.add(recipe);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return results;
    }

    // method for detecting if results are cached already or if its an initial search
    private boolean resultsNotCached(SearchType cache, int pageNumber) {
        return resultsCache.get(cache) == null || pageNumber == 1;
    }

    // method for deleting a recipe. returns a boolean for whether or not the recipe was successfully deleted
    public void deleteRecipe(String recipeName) {
        List<Recipe> recipes = getCurrentRecipes();
        List<Recipe> recipesToRemove = new ArrayList<>();
        //search for recipes to remove
        recipes.forEach(recipe -> {
            if (recipe.name.equals(recipeName)) {
                recipesToRemove.add(recipe);
            }
        });

        // dont rewrite if no matching recipes were found
        if (recipesToRemove.size() <= 0) return;

        // remove the recipe(s)
        recipes.removeAll(recipesToRemove);

        // rewrite database with reduced list
        try {
            if (database.delete()) {
                database = new File("recipeRepository.txt");

            } else {
                System.out.println("failed to delete recipe");
                return;
            }
            FileWriter writer = new FileWriter(database, true);
            recipes.forEach(recipe -> {
                try {
                    writer.write(recipe.toData());
                    writer.write("\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        resultsCache.clear();
        return;
    }
}
